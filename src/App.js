import React, { Component } from "react";
import todosList  from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
    newInput: ""
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
}

  addTodo = (e) => {
    if (e.keyCode === 13) {
      const newTodos = this.state.todos;
      const newTodo = {
        userId: 1,
        id: Math.random(),
        title: this.state.newInput,
        completed: false,
      };
      newTodos.push(newTodo);
      this.setState({ todos: newTodos, newInput: "" });
    }
  }
    markComplete = (id) => (e) => {
      const newTodos = this.state.todos
      
      newTodos.forEach((todo) => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
      });
    
      this.setState({ todos: newTodos });
  };
  delTodo = (id) => (e) => {
    this.setState({todos: [...this.state.todos.filter(todo => todo.id !== id)]})
  }
  
  // delTodo = (id) => (e) => {
  //   const newTodos = this.state.todos.filter((todo) => todo.id !== id);
  //   this.setState({ todos: newTodos });
  // };
  clearCompleted = () => {
     this.setState({ todos: [...this.state.todos.filter(todo => todo.completed === false)] })
   }
  

    render() {
      return (
        <section className="todoapp">
          <header className="header">
            <h1>MY TODOS</h1>
            <input
              className="new-todo"
              name="newInput"
              value={this.state.newInput}
              onChange={this.handleChange}
              onKeyDown={this.addTodo}
              placeholder="What needs to be done?"
              autoFocus
            />
          </header>
          <TodoList
            todos={this.state.todos}
            markComplete={this.markComplete}
            delTodo={this.delTodo}
          />
          <footer className="footer">
            <span className="todo-count">
              <strong>0</strong> item(s) left
          </span>
            <button className="clear-completed" onClick={this.clearCompleted}>Clear completed</button>
          </footer>
        </section>
      );
    }
  }

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed} 
            onChange={this.props.markComplete}
            />
          <label>{this.props.title}</label>
          <button
            className="destroy" onClick={this.props.delTodo}/>
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem title={todo.title}
              completed={todo.completed}
              markComplete={this.props.markComplete(todo.id)}
              delTodo={this.props.delTodo(todo.id)}
              key={todo.id}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
